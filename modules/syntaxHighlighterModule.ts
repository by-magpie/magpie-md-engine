import hljs from "https://esm.sh/highlight.js@11.8.0/lib/core";
import { RenderModule, UTF8Bytes} from "../renderModule.ts";
import { ParseOptions } from "../mdParser.ts";
import { magpieMetadata } from "../headFormat.ts";

const hljsSupportedLang: Map<string, [string, string[]]> = new Map([
    ["arduino", 
        ["highlight.js@11.8.0/lib/languages/arduino", ["ino"]]],
    ["arm", 
        ["highlight.js@11.8.0/lib/languages/arm", ["armasm"]]],
    ["apache", 
        ["highlight.js@11.8.0/lib/languages/apache", ["apacheconf"]]],
    ["asciidoc", 
        ["highlight.js@11.8.0/lib/languages/asciidoc", []]],
    ["autohotkey", 
        ["highlight.js@11.8.0/lib/languages/autohotkey", []]],
    ["bash", 
        ["highlight.js@11.8.0/lib/languages/bash", ["sh"]]],
    ["basic", 
        ["highlight.js@11.8.0/lib/languages/basic", []]],
    //["bbcode", 
    //    ["", []]],
    ["brainfuck", 
        ["highlight.js@11.8.0/lib/languages/brainfuck", ["bf"]]],
    ["c", 
        ["highlight.js@11.8.0/lib/languages/c", []]],
    ["cpp", 
        ["highlight.js@11.8.0/lib/languages/cpp", ["c++"]]],
    ["cmake", 
        ["highlight.js@11.8.0/lib/languages/cmake", []]],
    ["css", 
        ["highlight.js@11.8.0/lib/languages/css", []]],
    ["coffee", 
        ["highlight.js@11.8.0/lib/languages/coffee", ["coffeescript"]]],
    //["curl", 
    //    ["", []]],
    ["dns", 
        ["highlight.js@11.8.0/lib/languages/dns", []]],
    ["docker", 
        ["highlight.js@11.8.0/lib/languages/docker", ["dockerfile"]]],
    ["bat", 
        ["highlight.js@11.8.0/lib/languages/bat", ["dos"]]],
    ["gcode", 
        ["highlight.js@11.8.0/lib/languages/gcode", []]],
    ["go", 
        ["highlight.js@11.8.0/lib/languages/go", ["golang"]]],
    ["graphql", 
        ["highlight.js@11.8.0/lib/languages/graphql", []]],
    ["xml", 
        ["highlight.js@11.8.0/lib/languages/xml", ["html", "svg", "rss", "atom", "xhtml"]]],
    ["http", 
        ["highlight.js@11.8.0/lib/languages/http", ["https"]]],
    //["hlsl", 
    //    ["", []]],
    ["ini", 
        ["highlight.js@11.8.0/lib/languages/ini", ["toml"]]],
    ["json", 
        ["highlight.js@11.8.0/lib/languages/json", []]],
    ["java", 
        ["highlight.js@11.8.0/lib/languages/java", []]],
    ["js", 
        ["highlight.js@11.8.0/lib/languages/javascript", ["javascript", "jsx"]]],
    ["kotlin", 
        ["highlight.js@11.8.0/lib/languages/kotlin", []]],
    ["lisp", 
        ["highlight.js@11.8.0/lib/languages/lisp", []]],
    ["lua", 
        ["highlight.js@11.8.0/lib/languages/lua", []]],
    ["makefile", 
        ["highlight.js@11.8.0/lib/languages/makefile", ["make"]]],
    ["markdown", 
        ["highlight.js@11.8.0/lib/languages/markdown", ["md"]]],
    ["mirc", 
        ["highlight.js@11.8.0/lib/languages/mirc", []]],
    //["never", 
    //    ["", []]],
    ["nginx", 
        ["highlight.js@11.8.0/lib/languages/nginx", ["nginxconf"]]],
    ["glsl", 
        ["highlight.js@11.8.0/lib/languages/glsl", []]],
    ["php", 
        ["highlight.js@11.8.0/lib/languages/php", []]],
    ["txt", 
        ["highlight.js@11.8.0/lib/languages/txt", []]],
    ["pgsql", 
        ["highlight.js@11.8.0/lib/languages/pgsql", ["postgres"]]],
    ["powershell", 
        ["highlight.js@11.8.0/lib/languages/powershell", ["ps", "ps"]]],
    ["prolog", 
        ["highlight.js@11.8.0/lib/languages/prolog", []]],
    ["python", 
        ["highlight.js@11.8.0/lib/languages/python", ["py", "py3"]]],
    ["rust", 
        ["highlight.js@11.8.0/lib/languages/rust", ["rs"]]],
    ["scss", 
        ["highlight.js@11.8.0/lib/languages/scss", []]],
    ["sql", 
        ["highlight.js@11.8.0/lib/languages/sql", []]],
    ["shell", 
        ["highlight.js@11.8.0/lib/languages/shell", ["console"]]],
    ["typescript", 
        ["highlight.js@11.8.0/lib/languages/typescript", ["ts", "tsx"]]],
    ["yaml", 
        ["highlight.js@11.8.0/lib/languages/yaml", ["yml"]]],
])

export class HighlighterRM extends RenderModule {
    markdownParserOptions: ParseOptions = {};
    langMap = new Map<string, string>();
    async enableLanguages(langs: string[]){
        return false;
    }

    async enableLanguage(lang: string) {
        const langData = hljsSupportedLang.get(lang);
        if(langData == undefined){
            return false;
        }
        const fullName = lang;
        const uri = `https://esm.sh/${langData[0]}`;
        const alais = langData[1];
        const imp = await import(uri);
        if(!imp.default){
            return false;
        }
        hljs.registerLanguage(fullName, imp.default);
        for(const lang of [fullName, ...alais]){
            this.langMap.set(lang, fullName);
        }
        return true;
    }

    preProcess? = undefined;
    postProcess? = undefined;
    /**
     * disabled for this module.
     */
    addPlugin() {}

    processString(lang: string, content: string){
        const fqln = this.langMap.get(lang);
        if(!fqln){
            return false;
        }
        return hljs.highlight(content, {language: fqln}).value;
    }

    process(lang: string, content: UTF8Bytes) {
        if(this.langMap.has(lang)){
            return this.processString(lang, content.toString());
        }
        return false;
    }
    
}