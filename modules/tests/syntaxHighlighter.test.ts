import {HighlighterRM} from "../syntaxHighlighterModule.ts";
Deno.test({
    name: "typescript Test",
    async fn() {
        const hlrm = new HighlighterRM();
        const added = await hlrm.enableLanguage("typescript");
        console.log(`Typescript import: ${added}`);
        const resA = hlrm.processString("typescript", `
        const name: string = "joshua";
        const time: number = 10;
        let ageTime = \`\${name} \${time + 10}\`;
        return true;`);
        console.log(`result A: \n${resA}`);
        const resB = hlrm.processString("ts", "for(const i of arr) { arr[i] = () => console.log(`${i} of ${arr[i]}`)}");
        console.log(`result B: \n${resB}`);
    }})