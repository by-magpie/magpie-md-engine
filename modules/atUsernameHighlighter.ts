import { magpieMetadata } from "../headFormat.ts";
import { ParseOptions } from "../mdParser.ts";
import { RenderModule } from "../renderModule.ts"

const extractLinksAndContent = /\B(\@\w+)\b/gm;

export class AtUsernameHighlighter extends RenderModule {
    tagFormat: string;
    constructor(tagFormat: string){
        super();
        this.tagFormat = tagFormat;
    }
    markdownParserOptions: ParseOptions = {};

    postProess = (_metadata: magpieMetadata, htmlContent: string): [string, boolean] => {
        return [htmlContent.replace(extractLinksAndContent, this.tagFormat), true];
    }

}