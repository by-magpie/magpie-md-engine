import { html } from "https://cdn.skypack.dev/-/lit-html@v2.2.3-xm7mKfzpHOFeww3lxxRH/dist=es2019,mode=imports/optimized/lit-html.js";
import { magpieMetadata } from "../headFormat.ts";
import { ParseOptions } from "../mdParser.ts";
import { RenderModule } from "../renderModule.ts"

const extractLinksAndContent = /<a href="##([^"]+?)">(.+?)<\/a>/gm;

const extractPartialStart = /\[([^\]]+?)\]\(##\<([^\)]+?)\)/gm;
const extractPartialEnd = /\[([^\]]+?)\]\(##\<\/([^\)]+?)\)/gm;
export class ClassesAsLinks extends RenderModule {
    markdownParserOptions: ParseOptions = {};
    readonly partial: boolean;
    constructor(options?: {allowHalfTags: boolean}) {
        const defaults = {
            allowHalfTags: false,
        }
        const { allowHalfTags } = {...defaults, ...options}
        super();
        this.partial = allowHalfTags;
    }

    preProcess = (_metadata: magpieMetadata, markdownContent: string): [string, boolean] => {
        if(this.partial){
            let content = markdownContent;
            content = content.replace(extractPartialEnd, "\n\n<!-- END-PART $1 ($2)-->\n\n</div>\n\n")
            content = content.replace(extractPartialStart, "\n\n<div class=\"$2\">\n\n<!-- START-PART $1 -->\n\n")
            return [content, true]
        }
        return [markdownContent, true];
    };

    postProess = (_metadata: magpieMetadata, htmlContent: string): [string, boolean] => {
        return [htmlContent.replace(extractLinksAndContent, "<span class=\"$1\">$2</span>"), true];
    }

}