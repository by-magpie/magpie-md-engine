import { RenderModule } from "../renderModule.ts"
import type { magpieMetadata } from "../headFormat.ts";
import type { ParseOptions, UTF8Bytes as UTF8BytesImp } from "../mdParser.ts";

export type handelerFunc = (content: UTF8BytesImp) => Uint8Array|string|null|undefined
export type UTF8Bytes = UTF8BytesImp;
export class PluginModule extends RenderModule {
    markdownParserOptions: ParseOptions = {}
    private plugins = new Map<string, handelerFunc>()

    addPlugin(pluginName: string, handeler: handelerFunc) {
        this.plugins.set(pluginName, handeler);
    }

    /**
     * Processes the actual code blocks.
     * @param lang the language for the code block
     * @param content the content of the code block
     * @returns Returns `false` if is can't provide a response and `null` if it needs to prevent further modules from processing the data. (`string` for actual data.)
     */
    process(lang: string, content: UTF8BytesImp): Uint8Array|string|null|undefined|false {
        const m = this.plugins.get(lang);
        if(m){
            return m(content);
        }
        return false;
    }
}