import type {handelerFunc, UTF8Bytes} from "../renderModule.ts";
export const rawText = (content: UTF8Bytes, addLineInFront?: boolean) => {
    const str = content.toString();
    const rep = str.replace(/\n/gm, "<br />");
    if(addLineInFront){
        return "<br />" + rep;
    }
    return rep;
}