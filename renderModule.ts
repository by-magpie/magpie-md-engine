
import type { magpieMetadata } from "./headFormat.ts";
import type { ParseOptions, UTF8Bytes as UTF8BytesImp } from "./mdParser.ts";

export type handelerFunc = (content: UTF8BytesImp) => Uint8Array|string|null|undefined
export type UTF8Bytes = UTF8BytesImp;
export abstract class RenderModule {

    abstract markdownParserOptions: ParseOptions;

    preProcess?: (metadata: magpieMetadata, markdownContent: string) => [string, boolean];

    postProess?: (metadata: magpieMetadata, htmlContent: string) => [string, boolean];

    /**
     * Processes the actual code blocks.
     * @param lang the language for the code block
     * @param content the content of the code block
     * @returns Returns `false` if is can't provide a response and `null` if it needs to prevent further modules from processing the data. (`string` for actual data.)
     */
    process(lang: string, content: UTF8BytesImp): Uint8Array|string|null|undefined|false {
        return false;
    }
}