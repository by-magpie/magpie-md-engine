import { metadataAndContent } from "./metadata.ts";
import { markdownParser, ParseOptions, UTF8Bytes } from "./mdParser.ts";
import { RenderModule } from "./renderModule.ts";
import type { magpieMetadata } from "./headFormat.ts";

export class MagpieMdEngine {
    public renderConfigs = new Map<string | false, RenderModule[]>()
    public renderDefault: RenderModule[];
    constructor(renderDefault: RenderModule[], renderConfigs?: Map<string, RenderModule[]>) {
        if(renderConfigs){
            this.renderConfigs = renderConfigs
        }
        this.renderDefault = renderDefault;
    }
    async ready() {
        await markdownParser.ready;
    }
    step1(page: string) {
        const {metadata: metadataRaw, markdown} = metadataAndContent(page);
        if(metadataRaw.length == 0){
            throw new Error("Insufficent Metadata!");
        }
        const metadata = metadataRaw as [magpieMetadata, ...unknown[]];
        return {metadata, markdown}; 
    }
    step2(
        [metadata, ...__rest]: [magpieMetadata, ...unknown[]],
        page: string
    ){
        let content = page;
        let noStop = true;
        const rendererOpt: string = metadata.renderer || "";
        const rendererCascade = this.renderConfigs.get(rendererOpt) || this.renderDefault;
        for(const module of rendererCascade){
            if(module.preProcess){
                [content, noStop] = module.preProcess(metadata, content);
                if(noStop){
                    continue;
                }
                break;
            }
        }
        return content;
    }
    step3(
        [metadata, ...__rest]: [magpieMetadata, ...unknown[]],
        markdownContent: string,
        mdParserOpts?: ParseOptions
    ) {
        const rendererOpt: string = metadata.renderer || "";
        const rendererCascade = this.renderConfigs.get(rendererOpt) || this.renderDefault;
        let fullOptions: ParseOptions = {
            format: "html"
        };
        for(const module of rendererCascade){
            fullOptions = {
                ...fullOptions,
                ...module.markdownParserOptions,
            }
        }
        fullOptions = {
            ...fullOptions,
            ...mdParserOpts,
            bytes: false,
            onCodeBlock: (langname :string, body :UTF8Bytes) => {return this.cascadeLanguages(rendererCascade, langname, body)}
        }
        const output = markdownParser.parse(markdownContent, fullOptions);
        if(output instanceof Uint8Array){
            throw new Error("markdown parser returned a uint8array?");
        }
        return output;
    }
    step4(
        [metadata, ...__rest]: [magpieMetadata, ...unknown[]],
        page: string
    ){
        let content = page;
        let noStop = true;
        const rendererOpt: string = metadata.renderer || "";
        const rendererCascade = this.renderConfigs.get(rendererOpt) || this.renderDefault;
        for(const module of rendererCascade){
            if(module.postProess){
                [content, noStop] = module.postProess(metadata, content);
                if(noStop){
                    continue;
                }
                break;
            }
        }
        return content;
    }
    cascadeLanguages(cascade: RenderModule[], lang: string, content: UTF8Bytes): Uint8Array|string|null|undefined {
        let resp:Uint8Array|string|null|undefined|false = false;
        for(const module of cascade){
            resp = module.process(lang, content);
            if(resp !== false){
                break;
            }
        }
        if(resp === false){
            return null;
        } else {
            return resp;
        }
    } 
}

