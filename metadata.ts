import {parseAll as yamlParser} from "https://deno.land/std@0.188.0/yaml/mod.ts";

const rxexp = /^((?:---\n(?:.|\n)+?)?\n---\n)((?:.|\n)+)/m;

export function metadataAndContent(fullFile: string) {
    const groups = rxexp.exec(fullFile);
    if(!groups){
        throw new Error(`Group Parse Error! (Group is '${groups}')`);
    }
    const yamls = groups[1];
    const markdown = groups[2];
    const metadata = yamlParser(yamls);
    if(!Array.isArray(metadata)){
        throw new Error("Invalid type of meta!");
    }
    return {metadata, markdown};
}