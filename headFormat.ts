export type UUID = string;
export interface magpieMetadata {
    uuid: UUID,
    title: string,
    subt?: string,
    tags?: string[],
    service?: string,
    nsfw?: boolean,
    preview?: boolean,
    renderer?: string,
    theme?: string,
  }