The idea of this is to have a generic rendering engine where the parsing engine can be swapped out, the renderer can be swapped out, and plugins can be swapped out.

Flow of the code:

Chassis is the core of the project. Everything is connected through it.

Bonnet is where the engine is called from. abs.engine is a abstract engine and engines are stored within the engines folder. The engine is what takes the markdown and generates the HTML from it.

Body is where the renderer is called from. abs.renderer is a abstract renderer and renderers are stored within the renderers folder. It modifies HTML content to add/modify/remove tags. Including links to images, and adding the metadata back in, and adding wrappers and style tags.

Trailer is where plugins are called from. abs.plugin is a abstract plugin and plugins are stored within the plugins folder. Plugins are designed to take modified tags and expand them to more full content. Mostly interactive content but it may also be syntax highlighting, or custom styleing, or other content modifications.